package primefactors;
// @author Vince Belanger

import java.util.Scanner;
import java.util.ArrayList;

public class Factors {
    
    public static ArrayList<Integer> factor(int n){
        ArrayList<Integer> factors = new ArrayList<>();
        if(n != 0)
            factors.add(1);
        if(n < 0)
            factors.add(-1);
        
        for(int i = 2; i < Math.abs(n); i++){
            if(n % i == 0){
                    factors.add(i);
                    if(n < 0)
                        factors.add(i * -1);
            }
        }
        
        if(n < 0)
            factors.add(n * -1);        
        if(n != 1)
            factors.add(n);

        return factors;
    }
    
    public static boolean isPrime(ArrayList<Integer> factors, int n){
        for(int f : factors){
            if((f != 1) && (f != n) && (n != 0))
                return true;
        }
        
        return false;
    }
    
    public static int combos(ArrayList<Integer> factors, int n){
        int combo = 0;
        
        if(n == 0)
            combo = 0;
        else if(factors.size() <= 2)
            combo = 1;
        else
            combo = factors.size() / 2;
        
        return combo;
    }
    
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int n;
        int combo;
        boolean prime;
        
        System.out.println("Factors (by Vince Belanger)");
        System.out.println("-------------------------------------------------------");
        System.out.println("Type in an integer to find out if a number is prime or \ncomposite, and see the factors.");
        System.out.print("\nEnter your integer: ");
        n = cin.nextInt();
        
        ArrayList<Integer> factors = factor(n);
        prime = isPrime(factors, n);
        combo = combos(factors, n);
        
        System.out.print("\n" + n + " is ");
        if((prime) && n > 0)
            System.out.println("composite");
        else if(n <= 0)
            System.out.println("neither prime nor composite");
        else
            System.out.println("prime");
        
        if(n < 0)
            System.out.println("Only positive integers can be prime or composite.");
        
        System.out.print("\n" + n + "'s factors are: ");
        if(n == 0)
            System.out.println("All integers");
        else
            System.out.println(factors);
        
        
        System.out.print("\nThere ");
        if(combo == 1)
            System.out.print("is 1 combination ");
        else
            System.out.print("are " + combo + " combinations ");
        System.out.println("of integers that multiply to equal " + n);
    }
}